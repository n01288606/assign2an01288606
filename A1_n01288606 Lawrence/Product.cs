﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace A1_n01288606_Lawrence
{
    public class Product
    {

        public string rentalType;
        public int numItems;
        public List<string> genre;

        public Product(List<string> g, string rt, int ni)
        {
            genre = g;
            rentalType = rt;
            numItems = ni;
        }
           
    } 
}