﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace A1_n01288606_Lawrence
{
    public class Lending
    {
        //customer
        //delivery
        //CustomerSatisfaction
        //products

        public Customer customer;
        public Delivery delivery;
        public CustomerSatisfaction customerSatisfaction;
        public Product product;

        public Lending( Customer c, Delivery d, CustomerSatisfaction s, Product p)
        {
            customer = c;
            delivery = d;
            customerSatisfaction = s;
            product = p;

        }

        public string SummarizeOrder()
        {
            char separator = ',';

            string summary = "<h1>Hold Summary</h1>" + "<br/>";
            summary += "Hold Date: " + DateTime.Now.ToString() + "<br/>";
            summary += "First Name: " + customer.CustomerFName + "<br/>";
            summary += "Last Name: " + customer.CustomerLName + "<br/>";
            summary += "Email: " + customer.CustomerEmail + "<br/>";
            summary += "Age Range: " + customer.CustomerAgerange + "<br/>";

            summary += "Rental Type: " + product.rentalType + "<br/>";


            summary += "Number of Items: " + product.numItems + "<br/>";
            summary += "Genre: " + String.Join(", ",product.genre) + "<br/>";
            summary += "Delivery to: " + delivery.MailingAddress + " at " + delivery.DeliveryTime.ToString() + "<br/>";
            summary += "Customer Frequency: " + customerSatisfaction.custFrequency + "<br/>";
            summary += "Company Improvement: " + String.Join(separator.ToString(), customerSatisfaction.servImprove) + "<br/>";

            return summary;
        }
        public double CalculateLending() 
        {
            double baseAmount = 0.10;
            double total = baseAmount;
            if (product.numItems==1)
            {
                total = 3*baseAmount;
            }else if (product.numItems==2)
            {
                total = 5*baseAmount;
            }else if (product.numItems==3)
            {
                total = 7*baseAmount;
            }

            total += product.genre.Count() * 1;

            return total;

       }





    }
}