﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace A1_n01288606_Lawrence
{
    public class Delivery
    {
        //Mailing Address
        //DeliveryTime

        private string mailingAddress;
        private string deliveryTime;

        public string MailingAddress
        {
            get { return mailingAddress; }
            set { mailingAddress = value; }
        }
        public string DeliveryTime
        {
            get { return deliveryTime; }
            set { deliveryTime = value; }

        }

        
    }
}