﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace A1_n01288606_Lawrence
{
    public class Customer
    {
        //first name
        //last name
        //email
        //age

        private string customerFName;
        private string customerLName;
        private string customerEmail;
        private string customerAgerange;

    


        public string CustomerFName
        {
            get { return customerFName; }
            set { customerFName = value; }
        }
        public string CustomerLName
        {
            get { return customerLName; }
            set { customerLName = value; }
        }
        public string CustomerEmail
        {
            get { return customerEmail; }
            set { customerEmail = value; }
        }
        public string CustomerAgerange
        {
            get { return customerAgerange; }
            set { customerAgerange = value; }
        }
        
    }
}