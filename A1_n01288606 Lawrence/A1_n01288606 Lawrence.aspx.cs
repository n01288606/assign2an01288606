﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace A1_n01288606_Lawrence
{
    public partial class A1_n01288606_Lawrence : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //5 classes
            //Customer.cs
            //Product.cs
            //Delivery.cs
            //CustomerSatisfaction.cs
            //lending.cs
            //add protected void lending(object sender,....







        }

        protected void subbutton_Click(object sender, EventArgs e)
        {
            //first check to make sure everything is valid
            if (!Page.IsValid)
            {
                return;
            }

            string firstName = fname.Text.ToString();
            string lastName = lname.Text.ToString();
            string email = clientEmail.Text.ToString();
            string ageRangeStr = ageRange.SelectedValue;

            Customer customer = new Customer();
            customer.CustomerFName = firstName;
            customer.CustomerLName = lastName;
            customer.CustomerEmail = email;
            customer.CustomerAgerange = ageRangeStr;

            string mailingAddress = mailAddress.Text.ToString();
            string time = times.SelectedItem.Text;

            Delivery newDelivery = new Delivery();
            newDelivery.MailingAddress = mailingAddress;
            newDelivery.DeliveryTime = time;

            string rentType = rentalType.SelectedValue;
            string itemNumber = itemnum.Text.ToString();

            List<string> genres = new List<string>();

            foreach (Control control in genre.Controls)
            {
                if (control.GetType() == typeof(CheckBox))
                {
                    CheckBox g = (CheckBox)control;
                    if (g.Checked)
                    {
                        genres.Add(g.Text);
                    }

                }
            }
            //Taken From In-Class Example as done by Christine Bittle

            int numItems = Int32.Parse(itemNumber);

            Product lendingProduct = new Product(genres, rentType, numItems);
            


            string service = custFrequency.SelectedValue;
            List<string> improve = new List<string>();

            foreach (Control control in servImprove.Controls)
            {
                if (control.GetType() == typeof(CheckBox))
                {
                    CheckBox Imp = (CheckBox)control;
                    if (Imp.Checked)
                    {
                        improve.Add(Imp.Text);
                    }

                }
            }
            //Taken From In-Class Example as done by Christine Bittle
            CustomerSatisfaction review = new CustomerSatisfaction(improve, service);

            Lending newLoan = new Lending(customer, newDelivery, review, lendingProduct);

            string summary = newLoan.SummarizeOrder();
            LoanSummary.InnerHtml = summary;

            LoanForm.Visible = false;
        }
    }
}