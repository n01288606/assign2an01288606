﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="A1_n01288606 Lawrence.aspx.cs" Inherits="A1_n01288606_Lawrence.A1_n01288606_Lawrence" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Lending Library</title>
</head>
<body>
    <form id="form1" runat="server"> 
        <div runat="server" id="LoanSummary">

        </div>
        <div runat="server" id="LoanForm">
            <p>Lending Library: Set your parameters and a mystery item will be selected </p>
            <label> First Name:</label><asp:TextBox runat="server" ID="fname" placeholder="First Name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Enter First Name" ControlToValidate="fname" ID="validatorfname"></asp:RequiredFieldValidator>
            <br />
            <br /><label>Last Name:</label><asp:TextBox runat="server" ID="lname" placeholder="Last Name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Enter Last Name" ControlToValidate="lname" ID="validatorlname"></asp:RequiredFieldValidator>
            <br />
            <br /><label>Email:</label><asp:TextBox runat="server" ID="clientEmail" placeholder="Email Address"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Enter Valid Email" ControlToValidate="clientEmail" ID="Validatoremail"></asp:RequiredFieldValidator>
           <asp:RegularExpressionValidator ID="EmailValidator"  runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="clientEmail" ErrorMessage="Invalid Email"></asp:RegularExpressionValidator>
            <br />
            <br /><label>Mailing Address:</label><asp:TextBox runat="server" ID="mailAddress" placeholder="Mailing Address"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Enter Mailing Address" ControlToValidate="mailAddress" ID="MailValidator"></asp:RequiredFieldValidator>

            <br />
            <br /><label>Rental Types:</label>
            <asp:RadioButtonList runat="server" ID="rentalType">
                <asp:ListItem Text="Video Games">Video Games</asp:ListItem>
                <asp:ListItem Text="Board Games">Board Games</asp:ListItem>
                <asp:ListItem Text="Books">Books</asp:ListItem>
            </asp:RadioButtonList> <br />

            <br /> <label>Number of Items:</label>
            <asp:TextBox runat="server" ID="itemnum" placeholder="Choose Amount of Items"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Enter an Amount" ControlToValidate="itemnum" ID="Itemvalidator"></asp:RequiredFieldValidator><br />
            <asp:RangeValidator runat="server" ControlToValidate="itemnum" Type="Integer" MinimumValue="1" MaximumValue="3" ErrorMessage="Please Choose a Quantity Between 1-3"></asp:RangeValidator>

            <br />
            <br /><label>Age Range:</label>
               <asp:RadioButtonList runat="server" ID="ageRange">
                <asp:ListItem Text="Child">Child (7-12)</asp:ListItem>
                <asp:ListItem Text="Teen">Teen (13-17)</asp:ListItem>
                <asp:ListItem Text="Mature">Mature (18+)</asp:ListItem>
            </asp:RadioButtonList> <br />
            
            
            <div id="genre" runat="server">
                <p>Pick Between 1-8 and Your Item Will Fall Into Any of Those Categories </p>
                <label>Genres:</label> 
                <asp:CheckBox runat="server" ID="genreho" Text="Horror"/>
                <asp:CheckBox runat="server" ID="genreact" Text="Action" />
                <asp:CheckBox runat="server" ID="genrero" Text="Romance" />
                <asp:CheckBox runat="server" ID="genrefan" Text="Fantasy" /> <br />
                <asp:CheckBox runat="server" ID="genremy" Text="Mystery" />
                <asp:CheckBox runat="server" ID="genread" Text="Adventure" />
                <asp:CheckBox runat="server" ID="genretr" Text="Tragedy" />
                <asp:CheckBox runat="server" ID="genresci" Text="Science-Fiction" />

            </div>

           <br /> <label>Delivery Times:</label>
            <asp:DropDownList runat="server" ID="times">
                <asp:ListItem Value="t1" Text="11:00AM"></asp:ListItem>
                <asp:ListItem Value="t2" Text="12:00PM"></asp:ListItem>
                <asp:ListItem Value="t3" Text="1:00PM"></asp:ListItem>
                <asp:ListItem Value="t4" Text="2:00PM"></asp:ListItem>
                <asp:ListItem Value="t5" Text="3:00PM"></asp:ListItem>
                <asp:ListItem Value="t6" Text="4:00PM"></asp:ListItem>
                <asp:ListItem Value="t7" Text="5:00PM"></asp:ListItem>
                <asp:ListItem Value="t8" Text="6:00PM"></asp:ListItem>
                <asp:ListItem Value="t9" Text="7:00PM"></asp:ListItem>
                <asp:ListItem Value="t10" Text="8:00PM"></asp:ListItem>
            </asp:DropDownList> 
            <br />
            <br /><label>Have You Used This Service Before?</label> <br />
            <asp:RadioButtonList runat="server" ID="custFrequency">
                <asp:ListItem Text="1-3">1-3 Times</asp:ListItem>
                <asp:ListItem Text="3-5">3-5 Times</asp:ListItem>
                <asp:ListItem Text="5-7">5-7 Times</asp:ListItem>
                <asp:ListItem Text="7+">7+ Times</asp:ListItem>
            </asp:RadioButtonList> <br />

            <br /><label>Where Can We Improve?</label> <br />
            <div id="servImprove" runat="server">
                <asp:CheckBox runat="server" ID="Impex" Text="Expand Genres"/>
                <asp:CheckBox runat="server" ID="Impti" Text="Delivery Times" />
                <asp:CheckBox runat="server" ID="Impcs" Text="Customer Service" /> 
            </div>

           <br/><asp:Button  runat="server" ID="subbutton" Text="Submit" OnClick="subbutton_Click" /> 
           
        </div>
        <asp:ValidationSummary runat="server" ID="ValidationSummary" />
    </form>
</body>
</html>
